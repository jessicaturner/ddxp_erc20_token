## dDXP_erc20_token

**Author(s):** Henry Woo (Ethereum developer), Milos (DL) Preocanin (DxpChain architect) </br>
**Deployed by:** Long Jie (DxpChain JS Developer)

## What is dDXP and why we need it ? 

'dDXP' stands for 'Decelerated DxpChain, for the moment $DXP moves out from its native tech, it becomes MUCH SLOWER, hence 'decelerated' and a turtle-like logo for 'd'. To expand $DXP to other ecosystems, being the oldest, fastest dPOS around, constantly being rejected from top ecosystems for the simple fact that it's 'not their native token.'

Since people started going crazy with 'wrapped' BTC who does have price of its own and has nothing even close to what we consider PEG around DxpChain, or line of 'synths' by new modern tech startups... We invented MPA's with oracles more than half decade ago, so it's time to show their markets what is the real meaning of PEG to an Asset and how Dxpchain does it.

We need it to enter, demostrate power and dominate their markets while presenting their technology as obsolete.

**TX cost per swap within Dxpchain blockchain:** ~ $0.10 (Basic account) TimeToComplete: 3 seconds fixed </br>
**TX cost per swap within Ethereum blockchain:** ~ $18-$198 (Uniswap, Metamask) TimeToComplete: ~ 3 minutes

### ERC20 Characteristics and details:
Capped, Mintable, Burnable, Pausable.

**OWNER/MINTER ADDRESS:** 0xb4a19FA2196519641959c2Dd316daBe667F3fBd0 </br>
**TOKEN CONTRACT ADDRESS:** 0x58e27acd119a13bb41c795b9362b6c6dd39e0f3b

**Precision:** 5 decimals

**Max mintable supply:** 3,600,570,502.00000 dDXP

**Initial Supply:** 20,000.00000 dDXP

### dDXP ERC20 contract is subject to copyright of DxpChain non-profit organization 
Move Institute, Slovenia (dxpchain.org & EU Trademark)

### dDXP ERC20 official and ONLY corporate Partner/representative is 
DxpChain Management Group Co., LTD. Thailand

Updates: 
- 18 May. 2021. Etherscan profile and token being updated.
- In process ddxp.network (main website for dDXP with main wallets, info and more links)
- In process ddxp.markets (swap website for dDXP to maintain PEG 1:1)
- In process ddxp.supply (page within main website for dDXP with supply info, account balances and history of swap)

Registration with CMC will happen after we have 3 pricefeeds (SWAP's) so we satisfy minimum requirements of CMC for listing of a token.
